/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fibonaccicalculator;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * Main Class that opens the socket to the server, sends the input and expects
 * the result to be a fibonacci number at the specified n^th position.
 * @author Chaitra Bhat
 */
public class FibonacciCalculatorMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        
        // Declare Class variables
        Socket fibServerSocket = null;
        int fibonacciIndex = 0;
        boolean validInput = false;
        
        // Enter the IP address as example, 192.168.*.*
        String serverIPAddress = JOptionPane.showInputDialog(
            "Enter IP Address of the system where Fibonacci Calculator Server is runnig:");
        
        //Initialize the server
        (new Thread(new FibonacciServer())).start();
        
        //Connect to the server running on port 9090
        try {
            fibServerSocket = new Socket(serverIPAddress,9090);
        } catch (IOException ex) {
            Logger.getLogger(FibonacciCalculatorMain.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //Accept a valid integer as the input
        while(!validInput){
            try{
                fibonacciIndex = Integer.parseInt(JOptionPane.showInputDialog(
                "Enter the index to compute Fibonacci Number:"));
                validInput = true;
            }catch(NumberFormatException e){
                JOptionPane.showMessageDialog(null, "Please Enter a valid Integer");
            }
        }
        
        //Send and recieve the data to/from the server
        Communicator comm = new Communicator(fibServerSocket);
        comm.write(fibonacciIndex);
        String fibonacciNumber = comm.read();
        fibServerSocket.close();
        
        //Display the fibonacci number as the result.
        JOptionPane.showMessageDialog(null, fibonacciNumber);
        System.exit(0);
    }
    
}
