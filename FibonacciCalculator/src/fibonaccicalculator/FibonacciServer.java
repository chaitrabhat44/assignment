/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fibonaccicalculator;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * SocketServer Thread that keeps listening forever for serving any incoming 
 * client requests
 * @author Chaitra Bhat
 */
public class FibonacciServer implements Runnable{
    private ServerSocket fibSocketListner = null;    
    ThreadPoolExecutor executor = null;              
    
    public FibonacciServer() throws IOException {
        
        //Initialize the variables
        fibSocketListner = new ServerSocket(9090); 
        executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();  
    }
    
    private void initListener() {
        while (true) {
            try {
                //Accept any incoming client connection
                Socket socket = fibSocketListner.accept();
                
                //Spawn a thread from the pool to achieve concurrency
                FibonacciTaskManager task = new FibonacciTaskManager(socket);
                
                //Task to calculate fibonacci number and comunicating back to client is handled as a seperate task
                executor.execute(task); 
            } catch (IOException ex) {
                Logger.getLogger(FibonacciServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void run() {
        initListener();
    }
    
    
    public void closeConnections(){
        try {
            executor.shutdown();
            fibSocketListner.close();
        } catch (IOException ex) {
            Logger.getLogger(FibonacciServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
