/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fibonaccicalculator;

import java.net.Socket;

/**
 * Class to calculate the fibonacci number and communicate that to the client
 * @author Chaitra Bhat
 */
public class FibonacciTaskManager implements Runnable {   
    private Socket clientSocket = null;  
    private Communicator comm = null;
    private int fibonacciIndex;
    
    
    public FibonacciTaskManager(Socket socket){
        //Init  variables
        clientSocket = socket;        
        comm = new Communicator(clientSocket);          
    }
    
    
    @Override
    public void run() {          
        String fibonacciIndex = comm.read();   
        comm.write(computeFibonacci(Integer.parseInt(fibonacciIndex)));
        comm.closeConnections();
    }
       
    private int computeFibonacci(int fibonacciIndex){
        if(fibonacciIndex ==0)
            return 0;
        else if(fibonacciIndex == 1)
            return 1;
        else
            return computeFibonacci(fibonacciIndex-1) + computeFibonacci(fibonacciIndex-2);
    }
    
}
