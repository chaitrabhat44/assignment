/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fibonaccicalculator;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class to perform all Socket operations such as read/write 
 * @author Chaitra Bhat
 */
public class Communicator {
    Socket commSocket = null;
    BufferedReader inputStream = null;
    PrintWriter outputStream = null;
    
    public Communicator(Socket socket){
        commSocket = socket;
    }
    
    public String read(){        
        try {
            inputStream = new BufferedReader(new InputStreamReader(commSocket.getInputStream()));
            String value =  inputStream.readLine();
            return value;
        } catch (IOException ex) {
            Logger.getLogger(Communicator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void write(String value){
        try {
            outputStream = new PrintWriter(commSocket.getOutputStream(), true);
            outputStream.println(value);
        } catch (IOException ex) {
            Logger.getLogger(Communicator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void write(int value){
        try {
            outputStream = new PrintWriter(commSocket.getOutputStream(), true);
            outputStream.println(value);
            
        } catch (IOException ex) {
            Logger.getLogger(Communicator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void closeConnections(){
        try {
            inputStream.close();
            outputStream.close();
            commSocket.close();
        } catch (IOException ex) {
            Logger.getLogger(Communicator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
