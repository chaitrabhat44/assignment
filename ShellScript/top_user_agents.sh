#! /bin/bash

# Get the directory name where log file is located. I am using the current directory.
my_dir=$(pwd)
echo $my_dir

# Assuming user_agents  are alphanumeric, may contain '.' as special charecter
user_agents=( $(grep -o '"[a-zA-Z0-9.]*"' $my_dir/log_file.txt | sed 's/"//g' | sort -u) )
echo $user_agents

# Loop through the user_agents to find the total number of occurances in log file for each of them
for i in "${user_agents[@]}"
   do
        # 'find' command will locate the log_file in the given direcory
        # 'grep' command will find all the occurances in of the word searched for
        # 'wc' command will count the total number of such occurances 
	count=$(find $my_dir -name "log_file.txt" -print|xargs grep $i | wc -l)
	agent="$count $i"
        # Redirect the occurances along with the user_agent names to a file, result.txt
	echo  $agent >> $my_dir/result.txt
   done

# Sort the result file to arrange the user_agents based on the total number of counts
# Redirect the result to sorted_result.txt
sort -r $my_dir/result.txt > $my_dir/sorted_result.txt

# Print the first 10 lines which are the top 10 user_agents
head -10 $my_dir/sorted_result.txt
